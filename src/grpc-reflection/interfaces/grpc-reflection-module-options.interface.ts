import { InjectionToken, ModuleMetadata, OptionalFactoryDependency, Type } from '@nestjs/common';
import { GrpcOptions } from '@nestjs/microservices';

export interface GrpcReflectionOptionsFactory {
  createGrpcReflectionOptions(): Promise<GrpcOptions> | GrpcOptions;
}

export interface GrpcReflectionModuleAsyncOptions extends Pick<ModuleMetadata, 'imports'> {
  useExisting?: Type<GrpcReflectionOptionsFactory>;
  useClass?: Type<GrpcReflectionOptionsFactory>;
  useFactory?: (...args: unknown[]) => Promise<GrpcOptions> | GrpcOptions;
  inject?: Array<InjectionToken | OptionalFactoryDependency>;
}
