import { Controller } from '@nestjs/common';

import { BaseV1GrpcReflectionController } from './v1.base.controller';
import {
  protobufPackage,
  ServerReflectionController,
  ServerReflectionControllerMethods,
  ServerReflectionRequest,
  ServerReflectionResponse,
} from '../proto/grpc/reflection/v1alpha/reflection';
import { Observable } from 'rxjs';

/** Implements the 'v1alpha' version of the gRPC Reflection API spec
 *
 * @see {@link https://github.com/grpc/grpc/blob/master/doc/server-reflection.md}
 */
@Controller(protobufPackage)
@ServerReflectionControllerMethods()
export class V1AlphaGrpcReflectionController
  extends BaseV1GrpcReflectionController
  implements ServerReflectionController
{
  serverReflectionInfo(
    request$: Observable<ServerReflectionRequest>,
  ): Observable<ServerReflectionResponse> {
    return super.serverReflectionInfo(request$); // required since the @Controller decorator can't apply to inherited methods
  }
}
