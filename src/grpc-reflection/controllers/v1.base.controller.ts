import { Observable, Subject } from 'rxjs';
import { objectToCamel, objectToSnake, toCamel } from 'ts-case-convert';

import * as protoLoader from '@grpc/proto-loader';
import { Inject } from '@nestjs/common';
import { GrpcOptions } from '@nestjs/microservices';

import {
  ServerReflectionController,
  ServerReflectionRequest,
  ServerReflectionResponse,
} from '../proto/grpc/reflection/v1/reflection';
import { GRPC_CONFIG_PROVIDER_TOKEN } from '../grpc-reflection.constants';
import { ReflectionV1Implementation } from '@grpc/reflection/build/src/implementations/reflection-v1';

/** Base class for alpha and final 'v1' versions of the gRPC reflection API
 *
 * note: the final version of the v1 spec was identical to the alpha version, so
 * this common class is used in order to share logic across multiple controllers
 * (this class itself is *not* a controller). See child classes for actual APIs
 *
 * @see {@link https://github.com/grpc/grpc/blob/master/doc/server-reflection.md}
 */
export class BaseV1GrpcReflectionController implements ServerReflectionController {
  private readonly grpcReflectionService: ReflectionV1Implementation;

  constructor(
    @Inject(GRPC_CONFIG_PROVIDER_TOKEN)
    private readonly grpcConfig: GrpcOptions,
  ) {
    const { protoPath, loader } = grpcConfig.options;
    const protoFiles = Array.isArray(protoPath) ? protoPath : [protoPath];
    const root = protoLoader.loadSync(protoFiles, loader);

    this.grpcReflectionService = new ReflectionV1Implementation(root);
  }

  /** Main method for providing information about the running gRPC server
   *
   * The spec defines this as a single streaming method so that the connection
   * stays open to the same running instance of the server for all follow-up
   * requests. This means that we can keep some kind of state about what we've
   * already to that client or not if we need to.
   */
  serverReflectionInfo(
    request$: Observable<ServerReflectionRequest>,
  ): Observable<ServerReflectionResponse> {
    const response$ = new Subject<ServerReflectionResponse>();

    const onComplete = () => response$.complete();
    const onNext = (rawMsg: ServerReflectionRequest): void => {
      /* Convert the message keys from snake_case to camelCase to deal with proto-loader's "keepCase" option. This is
       * necessary because this module will be loaded into someone else's gRPC environment which we don't have control
       * over. If they've set keepCase to 'true' then we should convert it anyways for ourselves for consistency. */
      let message = rawMsg;

      if (this.grpcConfig.options.loader?.keepCase) {
        message = objectToCamel(message);
        message['messageRequest'] = toCamel(message['messageRequest']);
      }

      const response = this.grpcReflectionService.handleServerReflectionRequest(message);

      /** Similar to above, we need to handle 'keepCase' as part of the server response as well */
      if (this.grpcConfig.options.loader?.keepCase) {
        const convertedResponse = objectToSnake(response);
        const fixedConvertedResponse = {
          ...convertedResponse,

          // the "snake case conversion process" mangles our file descriptors, so if we're including that then we need
          // to pull that back from the original response object
          file_descriptor_response: convertedResponse.file_descriptor_response
            ? {
                ...convertedResponse.file_descriptor_response,
                file_descriptor_proto: response.fileDescriptorResponse.fileDescriptorProto,
              }
            : convertedResponse.file_descriptor_response,
        };
        response$.next(fixedConvertedResponse as unknown as ServerReflectionResponse); // coerce back to "correct" type
      } else {
        response$.next(response as ServerReflectionResponse); // send as-is, no need to convert
      }
    };

    request$.subscribe({
      next: onNext,
      complete: onComplete,
    });

    return response$.asObservable();
  }
}
