import { Module } from '@nestjs/common';
import { GrpcReflectionModule } from '../../grpc-reflection';
import { GrpcClientOptions } from './grpc-client.options';
import { HelloModule } from '../common/hello.module';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    HelloModule,
    ConfigModule.forRoot({ isGlobal: true }),
    GrpcReflectionModule.registerAsync({
      useFactory: async (configService: ConfigService) => {
        const grpcClientOptions: GrpcClientOptions = new GrpcClientOptions(configService);
        return grpcClientOptions.getGRPCConfig;
      },
      inject: [ConfigService],
    }),
  ],
})
export class AppModule {}
