import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { HelloService } from './hello.service';

@Controller('sample')
export class HelloController {
  constructor(private readonly appService: HelloService) {}

  @GrpcMethod('SampleService', 'hello')
  hello(): { world: string; status: number } {
    return {
      world: this.appService.getHello(),
      status: 1,
    };
  }
}
